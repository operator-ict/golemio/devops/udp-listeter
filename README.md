# Udp Listeter
Příjmá UDP data a ty ukládá na S3 nebo vypisuje do logu.

## Local test

### server
```bash
docker run --rm --name udp -p 5000:5000/udp udp-listener
```

### client
```bash
nc -u localhost 5000
```
