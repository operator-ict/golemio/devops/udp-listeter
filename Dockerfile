FROM bitnami/python:3.9.12

WORKDIR /app

COPY requirements.txt app.py ./
RUN pip install -r requirements.txt

EXPOSE 5000/udp
CMD ["python", "-u", "app.py"]
